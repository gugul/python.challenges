import pathlib

documents_folder = pathlib.Path.cwd() / "practice_files" / "documents"
image_folder = pathlib.Path.cwd() / "practice_files" / "images"
image_extensions = [
    "*.png",
    "*.gif",
    "*.jpg"
]
image_folder.mkdir(exist_ok=True)

for path in documents_folder.rglob("*.*"):
    if path.suffix in image_extensions:
        path.replace(image_folder / path.name)
