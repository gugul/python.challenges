cats_with_hats = {cat: False for cat in range(1, 101)}

for round_no in range(1, 101):
    for cat in range(round_no, 101, round_no):
        cats_with_hats[cat] = not cats_with_hats[cat]

list_of_cats = cats_with_hats.items()
list_of_cats_in_hats = []
for cat, hats in cats_with_hats.items():
    if hats:
        list_of_cats_in_hats.append(cat)


print(f"Cats with hats: {list_of_cats_in_hats}")
