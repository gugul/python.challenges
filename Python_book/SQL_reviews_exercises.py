import sqlite3

values = (
    ("Benjamin Sisko", "Human", 40),
    ("Jadzia Dax", "Trill", 300),
    ("Kira Nerys", "Bajoran", 29),
)
query_create_table = """
CREATE TABLE Roster(
    Name TEXT,
    Species TEXT,
    Age INT
);
"""
with sqlite3.connect("test_database.db") as connection:
    cursor = connection.cursor()
    cursor.execute("DROP TABLE IF EXISTS Roster")
    cursor.execute(query_create_table)
    cursor.executemany("INSERT INTO Roster VALUES(?, ?, ?);", values)
    update_query = "UPDATE Roster SET Name=? WHERE Species=? AND Age=?;"
    updated_data = ("Ezri Dax", "Trill", 300)
    cursor.execute(update_query, updated_data)
    cursor.execute(
        'SELECT Name, Age FROM Roster WHERE Species = "Bajoran";'
    )
    for row in cursor.fetchall():
        print(row)

