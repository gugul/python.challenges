import pathlib

my_folder = pathlib.Path.home() / "my_folder"
my_folder.mkdir(exist_ok=True)
paths = [
    my_folder / "file1.txt",
    my_folder / "file2.txt",
    my_folder / "image1.png"
]
for path in paths:
    path.touch()

source = paths[2]
destination = my_folder / "image" / "image1.png"
image_directory = my_folder / "image"
image_directory.mkdir(exist_ok=True)
source.replace(destination)
paths[0].unlink()

import shutil

shutil.rmtree(my_folder)
