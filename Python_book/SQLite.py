import sqlite3

# sql = """
# DROP TABLE IF EXISTS People;
# CREATE TABLE People(
#     FirstName TEXT,
#     LastName TEXT,
#     Age INT
# );
# INSERT INTO People VALUES(
#     'Ron',
#     'Obvious',
#     '42'
# );"""
#
# with sqlite3.connect("test_database.db") as connection:
#     cursor = connection.cursor()
#     cursor.executescript(sql)

# Security issue
first_name = input("Enter your first name: ")
last_name = input("Enter your last name: ")
age = int(input("Enter your age: "))

query = (
    "INSERT INTO People Values"
    f"('{first_name}', '{last_name}', {age});" # Jeżeli dane z inputu będą zawierały apostrof mogą powodować błędy.
)
with sqlite3.connect("test_database.db") as connection:
    cursor = connection.cursor()
    cursor.execute(query)

# Aby uniknąć błędów należy parametryzować wpisy
first_name = input("Enter your first name: ")
last_name = input("Enter your last name: ")
age = int(input("Enter your age: "))
data = (first_name, last_name, age)

with sqlite3.connect("test_database.db") as connection:
    cursor = connection.cursor()
    cursor.execute("INSERT INTO People VALUES(?, ?, ?);", data)

# Update wiersza przy pomocy parametryzacji
cursor.execute(
    "UPDATE People SET Age=? WHERE FirstNAme=? AND LastName=?;", (45, 'Luigi', 'Vercotti')
)



