from pathlib import Path
import csv

daily_temperatures = [
    [68, 65, 68, 70, 74, 72],
    [67, 67, 70, 72, 72, 70],
    [68, 70, 74, 76, 74, 72],
]

file_path = Path.home() / "temperatures.csv"
with file_path.open(mode="w", encoding="utf-8", newline="") as file:
    writer = csv.writer(file)
    for temp_list in daily_temperatures:
        writer.writerow(temp_list)
file.close()

#  Reading from csv
with file_path.open(mode="r", encoding="utf-8", newline="") as file:
    reader = csv.reader(file)
    for row in reader:
        int_row = [int(value) for value in row]
        daily_temperatures.append(int_row)
file.close()

file_path = Path.home() / "employees.csv"
file = file_path.open(mode="r", encoding="utf-8", newline="")
reader = csv.DictReader(file)
print(reader.fieldnames)
for row in reader:
    print(row)
file.close()


def process_row(row):
    row["salary"] = float(row["salary"])
    return row


with file_path.open(mode="r", encoding="utf-8", newline="") as file:
    reader = csv.DictReader(file)
    for row in reader:
        print(process_row(row))

people = [
    {"name": "Veronica", "age": 29},
    {"name": "Audrey", "age": 32},
    {"name": "Sam", "age": 24},
]

file_path = Path.home() / "people.csv"
file = file_path.open(mode="w", encoding="utf-8", newline="")
writer = csv.DictWriter(file, fieldnames=people[0].keys())
writer.writeheader()
writer.writerows(people)
file.close()
