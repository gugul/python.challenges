from bs4 import BeautifulSoup
from urllib.request import urlopen

url = "http://olympus.realpython.org/profiles"
page = urlopen(url)
html = page.read().decode("utf-8")
soup = BeautifulSoup(html, "html.parser")

print(soup.get_text())
links = soup.find_all("a")

links_list = []
base_url = "http://olympus.realpython.org/"
for link in links:
    address = base_url + link["href"]
    links_list.append(address)
    print(address)
print(links_list)

for link in links_list:
    page = urlopen(link)
    html = page.read().decode("utf-8")
    soup = BeautifulSoup(html, "html.parser")
    print(soup.get_text())


