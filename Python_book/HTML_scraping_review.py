from urllib.request import urlopen
import re

url = "http://olympus.realpython.org/profiles/dionysus"
page = urlopen(url)
html = page.read().decode("utf-8")
print(html)
name_index = html.find("Name:")
name_end_index = html.find("/h2>")
name_start_index = name_index + len("Name: ")
color_index = html.find("Favorite Color:")
color_end_index = html.find("/center>")
color_start_index = color_index + len("Favorite Color: ")
print(name_index, name_start_index, name_end_index)

print(color_index, color_start_index, color_end_index)
name_solution = html[name_start_index:name_end_index]
print(name_solution)
color_solution = html[color_start_index:color_end_index]
striped_color = color_solution.strip("<")
print(striped_color)

