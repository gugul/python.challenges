import csv

import matplotlib.pyplot as plt
from matplotlib import pyplot

import numpy
from matplotlib import pyplot
from pathlib import Path

csv_filepath = Path.cwd() / "pirates.csv"
file = csv_filepath.open(mode="r", encoding="utf-8", newline="")
reader = csv.reader(file)
years = []
y_temperatures = []
x_pirates = []
for row in reader:
    y_temperatures.append(row[1])
    x_pirates.append(row[2])
    years.append(row[0])

y_temperatures = y_temperatures[1:]
x_pirates = x_pirates[1:]
years = years[1:]

pyplot.plot(x_pirates, y_temperatures, "r-o")
plt.title("Global temperature as a function of pirate population")
plt.xlabel("Total pirates")
plt.ylabel("Average global temperature")

for i in range(0, len(years)):
    plt.annotate(str(years[i]), xy=(x_pirates[i], y_temperatures[i]))
pyplot.show()




