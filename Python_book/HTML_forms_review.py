import mechanicalsoup

browser = mechanicalsoup.Browser()
login_url = "http://olympus.realpython.org/login"
login_page = browser.get(login_url)
login_html = login_page.soup

form = login_html.select("form")[0]
form.select("input")[0]["value"] = "zeus"
form.select("input")[1]["value"] = "ThunderDude"

profile_page = browser.submit(form, login_page.url)
title = profile_page.soup.title
print(f"Title: {title.text}")

login_page = browser.get(login_url)
login_title = login_page.soup.title
print(f"Title: {login_title.text}")

form = login_html.form
form.select("input")[0]["value"] = "wrong"
form.select("input")[1]["value"] = "password"
error_page = browser.submit(form, login_page.url)

if "Wrong username or password!" in error_page.soup.text:
    print("Login Failed.")
else:
    print("Login Succesful"5)

