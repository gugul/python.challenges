import pathlib
import csv

# numbers = [
#     [1, 2, 3, 4, 5],
#     [6, 7, 8, 9, 10],
#     [11, 12, 13, 14, 15],
# ]
#
# file_path = pathlib.Path.home() / "numbers.csv"
# with file_path.open(mode="w", encoding="utf-8", newline="") as file:
#     writer = csv.writer(file)
#     writer.writerows(numbers)
#
# file.close()

# numbers = []
#
# with file_path.open(mode="r", encoding="utf-8", newline="") as file:
#     reader = csv.reader(file)
#     for row in reader:
#         int_row = [int(value) for value in row]
#         numbers.append(int_row)
# print(numbers)
# file.close()

file_path = pathlib.Path.home() / "favorite_colors.csv"

favorite_colors = [
    {"name": "Joe", "favorite_color": "blue"},
    {"name": "Anne", "favorite_color": "green"},
    {"name": "Bailey", "favorite_color": "red"},
]

with file_path.open(mode="w", encoding="utf-8", newline="") as file:
    writer = csv.DictWriter(file, fieldnames=favorite_colors[0].keys())
    writer.writeheader()
    writer.writerows(favorite_colors)
file.close()

with file_path.open(mode="r", encoding="utf-8", newline="") as file:
    reader = csv.DictReader(file)
    for row in reader:
        print(row)
    file.close()

