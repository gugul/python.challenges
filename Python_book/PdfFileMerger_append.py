from PyPDF2 import PdfFileMerger
from pathlib import Path

pdf_merger = PdfFileMerger()
reports_dir = (Path.home() / "practice_files" / "expense_report")

expense_report = list(reports_dir.glob("*.pdf"))
expense_report.sort()

for path in reports_dir.glob("*.pdf"):
    print(path.name)

for path in expense_report:
    pdf_merger.append(str(path))

with Path("expense_report.pdf").open(mode="wb") as output_file:
    pdf_merger.write(output_file)



