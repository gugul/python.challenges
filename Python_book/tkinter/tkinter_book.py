import tkinter as tk

window = tk.Tk()
label = tk.Label(
    text="Engage!",
    foreground="white",
    background="black",
    width=10,
    height=10
)
button = tk.Button(
    text="Click me!",
    width=25,
    height=5,
    bg="blue",
    fg="yellow",
)
entry = tk.Entry(fg="yellow", bg="blue", width=50)
entry.pack()
name = entry.get()
print(name)
label.pack()
button.pack()
window.mainloop()
