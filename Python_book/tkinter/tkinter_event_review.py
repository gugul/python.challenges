import tkinter as tk
import random


# def change_bg_color():
#     # bg_color = button["bg"]
#     button["bg"] = random.choice(["red", "orange", "yellow", "blue", "green", "indigo", "violet"])
#
#
# window = tk.Tk()
# button = tk.Button(master=window, text="Click me", fg="black", command=change_bg_color)
# button.pack()
#
# window.mainloop()


def roll():
    label["text"] = f"{random.randint(1,6)}"


window = tk.Tk()

window.rowconfigure([0, 1], minsize=10, weight=1)
window.columnconfigure(0, minsize=5,  weight=1)

button = tk.Button(master=window, text="Roll", command=roll, padx=10, pady=5)
label = tk.Label(master=window, text="", padx=10, pady=5)
button.grid(row=0, column=0, sticky="nsew")
label.grid(row=1, column=0, sticky="nsew")

window.mainloop()
