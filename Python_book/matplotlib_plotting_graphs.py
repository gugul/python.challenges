from matplotlib import pyplot as plt
import numpy as np
from numpy import random


# xs = [1, 2, 3, 4, 5]
# ys = [3, -1, 4, 0, 6]
#
# plt.plot([1, 2, 3, 4, 5], "g-o")
# plt.plot([1, 2, 4, 8, 16], "b-^")
# plt.show()

# array = np.arange(1, 6)

# plt.plot(array)
# plt.show()

# data = np.arange(1, 21).reshape(5, 4)
# plt.plot(data.transpose())
# plt.show()

# days = np.arange(0, 21)
# other_site, real_python = days, days ** 2
#
# plt.plot(days, other_site)
# plt.plot(days, real_python)
# plt.xticks([0, 5, 10, 15, 20])
# plt.xlabel("Days of Reading")
# plt.ylabel("Amount of Python Learned")
# plt.title("Python Learned Reading Real Python vsd Other Site")
# plt.legend(["Other Site", "Real Python"])
# plt.show()

# centers = [1, 2, 3, 4, 5]
# tops = [2, 4, 6, 8, 10]
#
# plt.bar(centers, tops)
# plt.show()


# fruits = {
#     "apples": 10,
#     "oranges": 16,
#     "bananas": 9,
#     "pears": 4,
# }
#
# plt.bar(fruits.keys(), fruits.values())
# plt.show()


plt.hist(random.randn(10000), 20)
plt.savefig("bar.png")
plt.show()