#  Pathlib method
from pathlib import Path
path = Path.home() / "hello.txt"
path.touch()
file = path.open(mode="r", encoding="utf-8")
file.close()

#  Built-in open() Function
file_path = "C:/Users/apast/hello.txt"
file = open(file_path, mode="r", encoding="utf-8")
file.close()

with path.open(mode="r", encoding="utf-8") as file:
    #  Do something with the file
    file.close()

