from PyPDF2 import PdfFileMerger, PdfFileReader, PdfFileWriter
from pathlib import Path
import copy

file_1_path = Path.home() / "merge1.pdf"
file_2_path = Path.home() / "merge2.pdf"
file_3_path = Path.home() / "merge3.pdf"
file_merger = PdfFileMerger()

file_merger.append(str(file_1_path))
file_merger.append(str(file_2_path))
file_merger.merge(1, str(file_3_path))

with Path("concatenated.pdf").open(mode="wb") as output_file:
    file_merger.write(output_file)

file_pdf = Path.home() / "split_and_rotate.pdf"
file_reader = PdfFileReader(str(file_pdf))
file_writer = PdfFileWriter()

for page in file_reader.pages:
    page.rotateCounterClockwise(90)
    left_side = copy.deepcopy(page)
    right_side = copy.deepcopy(page)
    curr_coords = left_side.mediaBox.upperRight
    new_coords = (curr_coords[0] / 2, curr_coords[1])
    left_side.mediaBox.upperRight = new_coords
    right_side.mediaBox.upperLeft = new_coords
    file_writer.addPage(left_side)
    file_writer.addPage(right_side)

with Path("rotated.pdf").open(mode="wb") as output_file:
    file_writer.write(output_file)

pdf_path = Path.home() / "top_secret.pdf"
file_reader = PdfFileReader(str(pdf_path))
file_writer = PdfFileWriter()
file_writer.appendPagesFromReader(file_reader)
file_writer.encrypt(user_pwd="Unguessable")

with Path("top_secret_encrypted.pdf").open(mode="wb") as output_file:
    file_writer.write(output_file)

pdf_path = Path.cwd() / "top_secret_encrypted.pdf"
pdf_reader = PdfFileReader(str(pdf_path))
pdf_reader.decrypt(password="Unguessable")
first_page = pdf_reader.getPage(0)
print(first_page.extractText())

