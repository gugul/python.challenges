import pathlib

path = pathlib.Path.home() / "starships.txt"
starships = [
    "Discovery\n",
    "Enterprise\n",
    "Defiant\n",
    "Voyager\n"
]
path.touch()
path.exists()
path.is_file()
with path.open(mode="w", encoding="utf-8") as file:
    file.writelines(starships)
    file.close()

with path.open(mode="r", encoding="utf-8") as file:
    for ship in file.readlines():
        if ship.startswith("D"):
            print(ship, end="")

