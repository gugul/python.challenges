from pathlib import Path

new_dir = Path.home() / "new_directory"
new_dir.mkdir(exist_ok=True)  # Tworzy katalog, jeżeli już istnieje pomija działanie
print(new_dir.exists())
print(new_dir.is_dir())

nested_dir = new_dir / "folder_a" / "folder_b"
nested_dir.mkdir(parents=True,
                 exist_ok=True)  # Tworzy podkatalog oraz podkatalog wyższego poziomu nawet jeżeli nie istnieje

file_path = new_dir / "file1.txt"
print(file_path.exists())
file_path.touch()
print(file_path.exists())
print(file_path.is_file())
file_path = new_dir / "folder_c" / "file2.txt"
file_path.parent.mkdir(exist_ok=True)
file_path.touch()

# Iterating over directory contents
for path in new_dir.iterdir():
    print(path)

print(list(new_dir.iterdir()))

# Searching for files in directory | glob()
for path in new_dir.glob("*.txt"):
    print(path)

paths = [
    new_dir / "program1.py",
    new_dir / "program2.py",
    new_dir / "folder_a" / "program3.py",
    new_dir / "folder_a" / "folder_b" / "image1.jpg",
    new_dir / "folder_a" / "folder_b" / "image2.jpg",
]

for path in paths:
    path.touch()

print(list(new_dir.glob("*.py")))
print(list(new_dir.glob("*1*")))
print(list(new_dir.glob("program?.py")))
print(list(new_dir.glob("?older_?")))
print(list(new_dir.glob("*1.??")))
print(list(new_dir.glob("program[13].py")))
print(list(new_dir.glob("**/*.py")))  # Sprawdza również podkatalogi
print(list(new_dir.rglob("*.py")))  # To samo działanie co powyżej

# Moving and deleting files and folders
source = new_dir / "file1.txt"
destination = new_dir / "folder_a" / "file1.txt"
if not destination.exists():  # Zabezpieczenie przed nadpisaniem danych jeżeli istnieją
    source.replace(destination)

# source = new_dir / "folder_c"
# destination = new_dir / "folder_d"
# source.replace(destination)

file_path = new_dir / "program1.py"
file_path.unlink(missing_ok=True)  # Ignoruje exception

folder_d = new_dir / "folder_d"
# for path in folder_d.iterdir():
#     path.unlink()

# folder_d.rmdir()  # Usunięcie pustego katalogu

#  Usunięcie katalogu wraz z całą zawartością przy pomocy import shutil
import shutil
folder_a = new_dir / "folder_a"
shutil.rmtree(folder_a)

