import mechanicalsoup
import time

browser = mechanicalsoup.Browser()

for i in range(4):
    page = browser.get("http://olympus.realpython.org/dice")
    tag = page.soup.select("#result")[0]
    result = tag.text
    time_css = page.soup.select("#time")[0]
    time1 = time_css.text
    print(f"The result of your dice roll is: {result} at {time1}")
    if i < 3:
        time.sleep(10)



