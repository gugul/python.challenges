from reportlab.pdfgen.canvas import Canvas
from reportlab.lib.units import inch, cm
from reportlab.lib.pagesizes import LETTER, A4
from reportlab.lib.colors import blue

canvas = Canvas("hello.pdf", pagesize=LETTER)
# canvas = Canvas("hello.pdf", pagesize=(8.5 * inch, 792.0))
canvas.drawString(72, 72, "Hello, World")
canvas.save()

canvas = Canvas("font_example.pdf", pagesize=LETTER)
canvas.setFont("Times-Roman", 18)
canvas.setFillColor("blue")
canvas.drawString(1 * inch, 10 * inch, "Times New Roman (18 pt), blue")

canvas.save()