class Animal:
    position = 0

    def __init__(self, name, sex, age):
        self.name = name
        self.sex = sex
        self.age = age
        self.food_eaten = 0

    def __str__(self):
        if self.food_eaten < 1:
            return f"{self.name} is hungry!"
        else:
            return f"{self.talk()}"

    def talk(self, sound=None):
        if sound is None:
            return f"I am {self.name}!"
        else:
            return f"{self.name} says {sound}"

    def eat(self, food_in_kg):
        self.food_eaten += food_in_kg
        return self.food_eaten

    def walk(self, distance):
        self.position += distance
        return self.position


class Cow(Animal):
    def talk(self, sound="Muuuuuu!"):
        return super().talk(sound)


class Chicken(Animal):
    def talk(self, sound="Ko Ko Ko Ko, Kukuryku!!!"):
        return super().talk(sound)


class Sheep(Animal):
    def talk(self, sound="Meeeeeeeeee!"):
        return super().talk(sound)


milka = Cow("Milka", "female", 6)
shaun = Sheep("Shaun", "male", 3)
hultaj = Chicken("Hultaj", "male", 4)

print(milka.talk())
print(shaun.talk())
print(hultaj.talk())
milka.eat(3)
shaun.eat(0)
hultaj.eat(4)
milka.walk(6)
shaun.walk(62)
hultaj.walk(45)
print(f"{milka.name} is {milka.position} m from here.")
print(f"{milka.name} ate {milka.food_eaten} kg of food.")
print(shaun)
print(milka)
