import easygui as gui
from PyPDF2 import PdfFileReader, PdfFileWriter
from pathlib import Path

pdf_to_open = gui.fileopenbox(msg="Choose PDF file to extract", title="Extract PDF", default="*.pdf")
if pdf_to_open is None:
    exit()

starting_page = -1
while starting_page < 1:
    starting_page = int(gui.enterbox(msg="Enter first page to extract", title="PDF extraction"))
    if starting_page is None:
        exit()
    elif starting_page < 1:
        print("The entry is invalid")

ending_page = starting_page - 1
while ending_page < starting_page:
    ending_page = int(gui.enterbox(msg="Enter last page to extract", title="Pdf Extraction"))
    if ending_page is None:
        exit()
    elif ending_page < starting_page:
        print("Ending page needs to be higher than starting page. Enter proper page number")

while_flag = True
while while_flag:
    save_location = gui.filesavebox(msg="Enter file name", title="Save as", default="*.pdf")
    while_flag = False
    if save_location is None:
        exit()
    elif save_location == pdf_to_open:
        while_flag = True
        print("You can not overwrite existing file")

pdf_file = PdfFileReader(pdf_to_open)
pdf_extracted = PdfFileWriter()

for page in pdf_file.pages[starting_page - 1:ending_page]:
    pdf_extracted.addPage(page)

with Path(save_location).open(mode="wb") as output_file:
    pdf_extracted.write(output_file)
