def convert_cel_to_far(celsius_degree):
    fahrenheit_degree = celsius_degree * 9/5 + 32
    return fahrenheit_degree


def convert_far_to_cel(fahrenheit_degree):
    celsius_degree = (fahrenheit_degree - 32) * 5/9
    return celsius_degree


fahrenheit_input = float(input("Enter a temperature in degrees F: "))
print(f"{fahrenheit_input} degrees F = {convert_far_to_cel(fahrenheit_input):.2f} degrees C")
celsius_input = float(input("Enter a temperature in degrees C: "))
print(f"{celsius_input} degrees C = {convert_cel_to_far(celsius_input):.2f} degrees F")
