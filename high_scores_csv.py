import pathlib
import csv

file_path = pathlib.Path.home() / "scores.csv"

with file_path.open(mode="r", encoding="utf-8", newline="") as file:
    reader = csv.DictReader(file)
    for row in reader:
        scores = [row for row in reader]

high_scores = {}
for item in scores:
    name = item["name"]
    score = int(item["score"])
    if name not in high_scores:
        high_scores[name] = score
    else:
        if score > high_scores[name]:
            high_scores[name] = score

file_path = pathlib.Path.home() / "high_scores.csv"

with file_path.open(mode="w", encoding="utf-8", newline="") as file:
    writer = csv.DictWriter(file, fieldnames=["name", "high_score"])
    writer.writeheader()
    for name in high_scores:
        record_dict = {"name": name, "high_score": high_scores[name]}
        writer.writerow(record_dict)
file.close()
