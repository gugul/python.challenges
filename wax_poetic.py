import random

nouns = ["fossil", "horse", "aardvark", "judge", "chef", "mango", "extrovert", "gorilla"]
verbs = ["kicks", "jingles", "bounces", "slurps", "meows", "explodes", "curdles"]
adjectives = ["furry", "balding", "incredulous", "fragrant", "exuberant", "glistening"]
prepositions = ["against", "after", "into", "beneath", "upon", "for", "in", "like", "over", "within"]
adverbs = ["curiously", "extravagantly", "tantalizingly", "furiously", "sensuously"]
articles = ["A", "An"]

generated_nouns = []
generated_verbs = []
generated_adjectives = []
generated_prepositions = []
generated_adverbs = []
generated_article = []


def generate_words(list_of_words, number_of_words, generated_words_list):
    for i in range(number_of_words):
        generated_words_list.append(random.choice(list_of_words))


generate_words(nouns, 3, generated_nouns)
generate_words(verbs, 3, generated_verbs)
generate_words(adjectives, 3, generated_adjectives)
generate_words(prepositions, 2, generated_prepositions)
generate_words(adverbs, 2, generated_adverbs)
generate_words(articles, 1, generated_article)

print(f"""
{generated_article[0]} {generated_adjectives[0]}

{generated_article[0]} {generated_adjectives[0]} {generated_nouns[0]} {generated_verbs[0]} {generated_prepositions[0]} the {generated_adjectives[1]} {generated_nouns[1]}
{generated_adverbs[0]}, the {generated_nouns[0]} {generated_verbs[1]}
the {generated_nouns[1]} {generated_verbs[2]} {generated_prepositions[1]} a {generated_adjectives[2]} {generated_nouns[2]}
""")
