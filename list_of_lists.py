import statistics

universities = [
    ['California Institute of Technology', 2175, 37704],
    ['Harvard', 19627, 39849],
    ['Massachusetts Institute of Technology', 10566, 40732],
    ['Princeton', 7802, 37000],
    ['Rice', 5879, 35551],
    ['Stanford', 19535, 40569],
    ['Yale', 11701, 40500]
]

students = []
tuition = []


def enrollment_stats(list_of_lists):
    for uni in list_of_lists:
        university, no_of_students, tuition_fee = uni
        students.append(no_of_students)
        tuition.append(tuition_fee)
    return students, tuition


def mean(list_to_process):
    mean_of_list = sum(list_to_process) / len(list_to_process)
    return round(mean_of_list, 2)


def median(list_to_process):
    return statistics.median(list_to_process)


enrollment_stats(universities)

mean_students = mean(students)
median_students = median(students)
mean_tuition = mean(tuition)
median_tuition = median(tuition)

output_message = f"""
******************************
Total students:   {sum(students):,}
Total tuition:  $ {sum(tuition):,}

Student mean:     {mean_students:,.2f}
Student median:   {median_students:,}

Tuition mean:   $ {mean_tuition:,.2f}
Tuition median: $ {median_tuition:,} 
******************************
"""

print(output_message)

