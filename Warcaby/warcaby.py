from colorama import Fore, Back, Style, init

init(autoreset=True)


class Piece:
    def __init__(self, color):
        self.color = color
        self.damka = False

    def make_damka(self):
        self.damka = True

    def __str__(self):
        black = f"{Back.BLACK}X{Style.RESET_ALL}"
        white = f"O"
        black_damka = f"{Back.BLACK}D{Style.RESET_ALL}"
        white_damka = f"D"
        if self.damka is True:
            if self.color == "black":
                return black_damka
            else:
                return white_damka

        elif self.color == "black":
            return black
        else:
            return white


class Board:
    def __init__(self):
        empty_row = [None] * 8
        self.board = [empty_row.copy() for _ in range(8)]

    def initialize_pieces(self):
        for row in range(3):
            for col in range(8):
                if (row + col) % 2 == 1:
                    self.board[row][col] = Piece("black")
        for row in range(5, 8):
            for col in range(8):
                if (row + col) % 2 == 1:
                    self.board[row][col] = Piece("white")

    def print_board(self):
        print(f"      {Fore.BLUE}\033[4m 1 2 3 4 5 6 7 8\033[0m")
        for i, row in enumerate(self.board):

            row_display = ""
            for piece in row:
                if piece is not None:
                    formatted_piece = str(piece) + " "
                    row_display += formatted_piece
                else:
                    row_display += "-" + " "
            print(f"    {Fore.YELLOW}{i + 1}|{Style.RESET_ALL} {row_display}")
        print()

    def move_piece(self, start_row, start_col, end_row, end_col):
        piece = self.board[start_row][start_col]

        if self.is_valid_move(start_row, start_col, end_row, end_col, piece.color):
            self.board[start_row][start_col] = None
            self.board[end_row][end_col] = piece

            if end_row == 0 and piece.color == "white":
                piece.make_damka()
            elif end_row == 7 and piece.color == "black":
                piece.make_damka()

            if abs(end_row - start_row) == 2:
                middle_row = (start_row + end_row) // 2
                middle_col = (start_col + end_col) // 2
                self.board[middle_row][middle_col] = None

    def is_valid_move(self, start_row, start_col, end_row, end_col, color):
        piece = self.board[start_row][start_col]
        target_square = self.board[end_row][end_col]
        middle_row = (start_row + end_row) // 2
        middle_col = (start_col + end_col) // 2
        middle_piece = self.board[middle_row][middle_col]

        if (
                0 <= start_row < 8
                and 0 <= start_col < 8
                and 0 <= end_row < 8
                and 0 <= end_col < 8
                and piece is not None
                and target_square is None
                and piece.color == color
        ):
            if piece.damka:
                if abs(end_row - start_row) == 1 and abs(end_col - start_col) == 1:
                    return True
                elif abs(end_row - start_row) == 2 and abs(end_col - start_col) == 2:
                    return middle_piece is not None and middle_piece.color != piece.color
            elif piece.color == "white":
                if end_row - start_row == -1 and abs(end_col - start_col) == 1:
                    return True
                elif end_row - start_row == -2 and abs(end_col - start_col) == 2:
                    return middle_piece is not None and middle_piece.color != piece.color
            else:
                if end_row - start_row == 1 and abs(end_col - start_col) == 1:
                    return True
                elif end_row - start_row == 2 and abs(end_col - start_col) == 2:
                    return middle_piece is not None and middle_piece.color != color
        return False

    def are_pieces_remaining(self, color):
        for row in self.board:
            for piece in row:
                if piece is not None and piece.color == color:
                    return True

    def has_piece(self, row, col):
        if 0 <= row < 8 and 0 <= col < 8:
            return self.board[row][col] is not None
        return False


class Game:
    def __init__(self):
        self.board = Board()
        self.current_player = "white"
        self.game_over = False

    def switch_player(self):
        if self.current_player == "white":
            self.current_player = "black"
        else:
            self.current_player = "white"

    def get_integer_input(self, prompt):
        while True:
            try:
                value = int(input(prompt))
                return value
            except ValueError:
                print(f"        {Fore.RED}Invalid input. Please enter a valid integer.")

    def play(self):
        print(f"""

        {Fore.YELLOW}{Back.BLACK}CHECKERS GAME{Style.RESET_ALL}
      player vs player

                """)
        self.board.initialize_pieces()
        while not self.game_over:
            self.board.print_board()
            print(f"    Current player: {Fore.BLACK}{Back.WHITE} {self.current_player} {Style.RESET_ALL}")

            start_row = self.get_integer_input(f"        {Fore.YELLOW}Enter starting row: ") - 1
            start_col = self.get_integer_input(f"        {Fore.BLUE}Enter starting column: ") - 1
            print()
            end_row = self.get_integer_input(f"        {Fore.YELLOW}Enter ending row: ") - 1
            end_col = self.get_integer_input(f"        {Fore.BLUE}Enter ending column: ") - 1
            print()

            if self.board.has_piece(start_row, start_col):
                if self.board.is_valid_move(start_row, start_col, end_row, end_col, self.current_player):
                    self.board.move_piece(start_row, start_col, end_row, end_col)
                    self.switch_player()

                    if not self.board.are_pieces_remaining("white"):
                        self.game_over = True
                        print("Black player wins!")
                    elif not self.board.are_pieces_remaining("black"):
                        self.game_over = True
                        print("White player wins!")
                else:
                    print(f"      {Fore.RED}Invalid move. Try again.")
                    print()
            else:
                print(f"      {Fore.RED}There is nothing on the given field. Try again!")
                print()
        self.board.print_board()
        print("Game over")


if __name__ == '__main__':
    game = Game()
    game.play()
