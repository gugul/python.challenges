import random

candidate_a_votes_in_regions = [0, 0, 0]
candidate_b_votes_in_regions = [0, 0, 0]
candidate_a_won_regions = 0
candidate_b_won_regions = 0
regions_chances = [0.87, 0.65, 0.17]


def election_by_chances(candidate_chances):
    if random.random() < candidate_chances:
        return "candidate_a"
    else:
        return "candidate_b"


for votes in range(10_000):
    for i in range(3):
        if election_by_chances(regions_chances[i]) == "candidate_a":
            candidate_a_votes_in_regions[i] += 1
        else:
            candidate_b_votes_in_regions[i] += 1
        if candidate_a_votes_in_regions[i] == 5001:
            candidate_a_won_regions += 1
        elif candidate_b_votes_in_regions[i] == 5001:
            candidate_b_won_regions += 1
if candidate_a_won_regions == 2:
    print("Candidate A won the elections")
elif candidate_b_won_regions == 2:
    print("Candidate B won the elections")

for i in range(3):
    print(f"In Region {i + 1} Candidate A received {candidate_a_votes_in_regions[i]} votes and Candidate B {candidate_b_votes_in_regions[i]}")
