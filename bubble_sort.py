import random


def generate_list_of_numbers(numbers):
    list_name = []
    for i in range(numbers):
        list_name.append(random.randint(0, 99))
    return list_name


def bubble_sort(given_list):
    n = len(given_list)
    flag = True
    while flag:
        for iteration in range(n - 1):
            for i in range(n - 1 - iteration):
                if given_list[i] > given_list[i + 1]:
                    temp = given_list[i]
                    given_list[i] = given_list[i + 1]
                    given_list[i + 1] = temp
                else:
                    flag = False
    return given_list


generated_list = generate_list_of_numbers(10)
sorted_list = bubble_sort(generated_list)
print(sorted_list)
