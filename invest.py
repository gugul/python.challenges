def invest(amount, rate, years):
    for year in range(years):
        new_amount = amount + amount * rate
        print(f"year {year}: ${new_amount:.2f}")
        amount = new_amount


amount = float(input("Enter initial amount $: "))
rate = float(input("Enter annual percentage rate: "))
years = int(input("Enter number of years: "))
invest(amount, rate, years)
