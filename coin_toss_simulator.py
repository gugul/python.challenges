import random

heads_tally = 0
tails_tally = 0
flips_no = 0
flips_number_to_have_head_and_tail = []


def coin_flip():
    if random.randint(0, 1) == 0:
        return "heads"
    else:
        return "tails"


for i in range(10_000):
    flips_no += 1
    if coin_flip() == "heads":
        heads_tally = 1
    else:
        tails_tally = 1
    if heads_tally == 1 and tails_tally == 1:
        flips_number_to_have_head_and_tail.append(flips_no)
        heads_tally = 0
        tails_tally = 0
        flips_no = 0

print(f"Average number of attempts is: {sum(flips_number_to_have_head_and_tail) / len(flips_number_to_have_head_and_tail):.0f}")

