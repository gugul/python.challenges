class SchoolMember:
    def __init__(self, name, surname, age):
        self.name = name
        self.surname = surname
        self.age = age
        self.active = True

    def __str__(self):
        return f"{self.name} {self.surname} is {self.age} years old."

    def birthday(self):
        self.age += 1


class Teacher(SchoolMember):
    def __init__(self, name, surname, age, subject):
        super().__init__(name, surname, age)
        self.subject = subject

    def info(self):
        if self.active is True:
            print(f"{self.name} {self.surname} is {self.subject} teacher and is {self.age} years old.")
        else:
            print("This teacher is not working anymore.")

    def retire(self):
        self.active = False


class Student(SchoolMember):
    def __init__(self, name, surname, age, class_no):
        super().__init__(name, surname, age)
        self.class_no = class_no
        self.math_grade = []
        self.science_grade = []
        self.english_grade = []

    def info(self):
        print(f"{self.name} {self.surname} is a student of {self.class_no} class and is {self.age} years old.")

    def promote(self):
        self.class_no += 1

    def suspend(self):
        self.active = False

    def distinct(self):
        pass

    def add_grade(self, subject, grade):
        if subject == "math":
            self.math_grade.append(grade)
        elif subject == "science":
            self.science_grade.append(grade)
        elif subject == "english":
            self.english_grade.append(grade)

    def show_all_grades(self):
        print(f"""{self.name} {self.surname}'s grades:
               Math: {self.math_grade} Average: {sum(self.math_grade) / len(self.math_grade)}
               Science: {self.science_grade} Average: {sum(self.science_grade) / len(self.science_grade)}
               English: {self.english_grade} Average: Average: {sum(self.english_grade) / len(self.english_grade)}
               """)


s = Student("Tony", "Hanks", 8, 3)
s.add_grade("math", 4)
s.add_grade("math", 3)
s.add_grade("math", 5)
s.add_grade("science", 3)
s.add_grade("science", 5)
s.add_grade("english", 6)
s.add_grade("english", 2)
s.show_all_grades()
t = Teacher("Julia", "Roberts", 45, "Math")
t.info()
t.retire()
t.info()
