import random
import tkinter as tk
from tkinter.filedialog import asksaveasfilename


def generate_poem():
    nouns.clear()
    verbs.clear()
    adjectives.clear()
    prepositions.clear()
    adverbs.clear()

    nouns_entered = list(ent_nouns.get().split(","))
    verbs_entered = list(ent_verbs.get().split(","))
    adjectives_entered = list(ent_adjectives.get().split(","))
    prepositions_entered = list(ent_prepositions.get().split(","))
    adverbs_entered = list(ent_adverbs.get().split(","))

    if len(nouns_entered) < 3 or len(verbs_entered) < 3 or len(adjectives_entered) < 3 or len(
            prepositions_entered) < 3 or "" in adverbs_entered:
        generated_poem["text"] = "Please enter at least 3 nouns, verbs, adjectives, prepositions and 1 adverb."

    nouns.append(random.sample(nouns_entered, 3))
    verbs.append(random.sample(verbs_entered, 3))
    adverbs.append(random.sample(adverbs_entered, 1))
    adjectives.append(random.sample(adjectives_entered, 3))
    prepositions.append(random.sample(prepositions_entered, 3))
    poem = f"""
    A {adjectives[0][0]} {nouns[0][0]}

    A {adjectives[0][0]} {nouns[0][0]} {verbs[0][0]} {prepositions[0][0]} the {adjectives[0][1]} {nouns[0][1]}
    {adverbs[0][0]}, the {nouns[0][0]} {verbs[0][1]}
    the {nouns[0][1]} {verbs[0][2]} {prepositions[0][1]} a {adjectives[0][2]} {nouns[0][2]}
    """
    generated_poem["text"] = poem


def save_poem():
    filepath = asksaveasfilename(
        defaultextension="txt",
        filetypes=[("Text Files", "*.txt"), ("All Files", "*.*")]
    )
    if not filepath:
        return

    with open(filepath, "w") as output_file:
        output_file.write(generated_poem["text"])


window = tk.Tk()
window.title("Make your own poem!")

frm_header = tk.Frame()
lbl_main = tk.Label(master=frm_header, text="Enter your favorite word, separated by commas.")
lbl_main.pack()
frm_header.pack(padx=5, pady=5)

frm_input = tk.Frame()
lbl_ent_nouns = tk.Label(master=frm_input, text="Nouns:")
lbl_ent_verbs = tk.Label(master=frm_input, text="Verbs:")
lbl_ent_adjectives = tk.Label(master=frm_input, text="Adjectives:")
lbl_ent_prepositions = tk.Label(master=frm_input, text="Prepositions:")
lbl_ent_adverbs = tk.Label(master=frm_input, text="Adverbs:")

ent_nouns = tk.Entry(master=frm_input, width=90)
ent_verbs = tk.Entry(master=frm_input, width=90)
ent_adjectives = tk.Entry(master=frm_input, width=90)
ent_prepositions = tk.Entry(master=frm_input, width=90)
ent_adverbs = tk.Entry(master=frm_input, width=90)

lbl_ent_nouns.grid(row=0, column=0, sticky="e")
ent_nouns.grid(row=0, column=1)
lbl_ent_verbs.grid(row=1, column=0, sticky="e")
ent_verbs.grid(row=1, column=1)
lbl_ent_adjectives.grid(row=2, column=0, sticky="e")
ent_adjectives.grid(row=2, column=1)
lbl_ent_prepositions.grid(row=3, column=0, sticky="e")
ent_prepositions.grid(row=3, column=1)
lbl_ent_adverbs.grid(row=4, column=0, sticky="e")
ent_adverbs.grid(row=4, column=1)
frm_input.pack(padx=5, pady=5)

frm_generate = tk.Frame(master=window)
frm_generate.pack(pady=10)
btn_generate = tk.Button(master=frm_generate, text="Generate", command=generate_poem)
btn_generate.pack()

frm_display = tk.Frame(master=window, relief="groove", borderwidth=5)
txt_display_result = tk.Label(master=frm_display, text="Your poem")
txt_display_result.pack(pady=10)
generated_poem = tk.Label(master=frm_display, text="Press the 'Generate' button to generate poem.")
generated_poem.pack(padx=5)
btn_save = tk.Button(master=frm_display, text="Save to file", command=save_poem)
btn_save.pack(pady=10)
frm_display.pack(fill=tk.X, padx=5, pady=5)

nouns = []
verbs = []
adverbs = []
adjectives = []
prepositions = []

window.mainloop()
