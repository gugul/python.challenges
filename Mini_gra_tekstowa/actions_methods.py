import pathlib


def fight(player, enemy):
    if enemy.power >= player.power:
        print(f"""You have been defeated and run! 
        
                          GAME OVER
""")
        player.winner = False
    else:
        print("You defeated the enemy!!! The beast is dead and can not stop you anymore")
        player.winner = True


def open_menu_from_file(file_name):
    file_path = pathlib.Path.cwd() / file_name
    with file_path.open("r", encoding="utf-8") as file:
        for no, line in enumerate(file.readlines()):
            print(no + 1, line, end="")
    file.close()


def read_text_from_file(file_name):
    file_path = pathlib.Path.cwd() / file_name
    with file_path.open("r", encoding="utf-8") as file:
        return file.read()


def open_file_as_list(file_name):
    file_path = pathlib.Path.cwd() / file_name
    with file_path.open("r", encoding="utf-8") as file:
        list = file.readlines()
        final_list = [item.strip() for item in list]
        return final_list
