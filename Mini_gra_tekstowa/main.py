from time import sleep
from characters_classes import Player, Enemy
from actions_methods import fight, open_menu_from_file, read_text_from_file, open_file_as_list


room_1 = open_file_as_list("room_1.txt")
room_2 = open_file_as_list("room_1.txt")
mystery_chest = open_file_as_list("mystery_chest.txt")
beast_defeated = False

player_name = input("Enter your name: ")

player = Player(player_name)
dragon = Enemy("Dragon", 5)

print(f"""
{player_name.capitalize()}. You wake up in a dark room. Disoriented, you wonder what you should do.
""")
while True:
    print()
    first_input = input("What should I do? Enter WAIT or SEARCH: ")
    if first_input.upper() == "WAIT":
        print("Waiting...")
        sleep(3)
        continue
    elif first_input.upper() == "SEARCH":
        print("You found: ")
        break
print()
while player.position == 1:
    player.search(room_1)
    print()
    open_input = input("What would you like to use? Enter number:  ")
    if open_input == "1":  # Mystery chest
        if "Sword" not in player.inventory:
            print(f"""You open the chest, and before your eyes appears a great sword
            
            You take a sword and feel now much stronger
            {player_name} power + 3
            
            """)
            player.power += 3
            player.inventory.append("Sword")
            continue
        else:
            print("""
            
            The chest is empty.
           
            """)

    elif open_input == "2":  # Door
        print("""
        
        You enter a new room and notice... 
        
        """)
        open_menu_from_file("room_2.txt")
        print()
        room_2_input = int(input("What do you choose? "))
        if room_2_input == 1:
            print()
            print("You encounter a terrifying beast. The beast attacks!")
            sleep(1)
            if "Sword" in player.inventory:
                print("""
                
                You draw your sword and face your opponent.
                
                """)

            else:
                print("""
                
                You try to defend yourself with your bare hands.
                
                """)
            sleep(3)
            fight(player, dragon)
            print()
            player.position = 2

        elif room_2_input == 2:
            if player.winner is True:
                print(read_text_from_file("final_passive_text.txt"))
                quit()
            else:
                print(read_text_from_file("beast_door_text.txt"))
                quit()
    else:
        print("Type correctly a proper number.")
        continue

if player.winner is True:
    while True:
        final_input = input("Do you want to enter Decorated door? YES or NO: ")
        if final_input.upper() == "YES":
            print(read_text_from_file("final_winning_text.txt"))
            quit()
        elif final_input.upper() == "NO":
            print(read_text_from_file("final_passive_text.txt"))
            quit()
        else:
            print("Only YES or NO! Try once more")
else:
    quit()
