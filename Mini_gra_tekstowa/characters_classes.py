class Player:
    def __init__(self, name):
        self.name = name
        self.power = 4
        self.inventory = []
        self.winner = False
        self.position = 1

    def search(self, room):
        for i, item in enumerate(room):
            print(f"{i + 1}. {item}")

    def take(self, tool):
        self.inventory.append(tool)

    def open(self, object):
        if "Door" in object:
            print("You entered to the next room")
        else:
            for i, item in enumerate(object):
                print(f"You found {i + 1}. {item}")


class Enemy:
    def __init__(self, name, power):
        self.name = name
        self.power = power
