class Animal:
    def __init__(self, sex, age):
        self.sex = sex
        self.age = age
        self.food_eaten_per_day = 0

    def feed(self, food_in_kg_per_day):
        self.food_eaten_per_day += food_in_kg_per_day


class Cows(Animal):
    def __init__(self, sex, age):
        super().__init__(sex, age)
        self.milk_given = 0

    def milk(self, milk_given_per_day):
        self.milk_given += milk_given_per_day


class Chickens(Animal):
    def __init__(self, sex, age):
        super().__init__(sex, age)
        self.laid_eggs = 0

    def collected_eggs(self, laid_eggs):
        self.laid_eggs += laid_eggs


class Sheep(Animal):
    def __init__(self, sex, age):
        super().__init__(sex, age)
        self.wool_collected = 0

    def wool(self, wool_collected):
        self.wool_collected += wool_collected


milka = Cows("male", 5)
milka.milk(5)
milka.feed(3)
print(f"Cow ate {milka.food_eaten_per_day} kg of hay and gave {milka.milk_given}l of milk.")

chicken = Chickens("Female", 2)
chicken.feed(1)
chicken.collected_eggs(3)
print(f"Chicken ate {chicken.food_eaten_per_day}kg  of seed and laid {chicken.laid_eggs} eggs.")

sheep = Sheep("Male", 6)
sheep.feed(3)
sheep.wool(6)
print(f"Sheep ate {sheep.food_eaten_per_day}kg of hay and gave {sheep.wool_collected}kg of wool.")


