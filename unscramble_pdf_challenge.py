from pathlib import Path
from PyPDF2 import PdfFileReader, PdfFileWriter

pdf_path = Path.home() / "scrambled.pdf"
pdf_reader = PdfFileReader(str(pdf_path))
pdf_writer = PdfFileWriter()
page_no = 0
while pdf_reader.getNumPages() != pdf_writer.getNumPages():
    for page in pdf_reader.pages:
        page_index = int(page.extractText().replace("!", "")) - 1
        if page_index == page_no:
            page_rotation = page["/Rotate"]
            if page_rotation != 0:
                page.rotateCounterClockwise(page_rotation)
            pdf_writer.addPage(page)
            page_no += 1


output_path = Path.home() / "unscrambled.pdf"
with output_path.open(mode="wb") as output_file:
    pdf_writer.write(output_file)
