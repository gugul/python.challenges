from pathlib import Path
from PyPDF2 import PdfFileReader, PdfFileWriter


class PdfFileSplitter:
    def __init__(self, path_string):
        self.path_string = path_string
        self.writer1 = None
        self.writer2 = None

    def split(self, breakpoint):
        self.writer1 = PdfFileReader(str(self.path_string)).pages[:breakpoint]
        self.writer2 = PdfFileReader(str(self.path_string)).pages[breakpoint:]

    def write(self, filename):
        pdf_writer1 = PdfFileWriter()
        for page in self.writer1:
            pdf_writer1.addPage(page)
        with Path(filename + "_1.pdf").open(mode="wb") as output_file:
            pdf_writer1.write(output_file)
        pdf_writer2 = PdfFileWriter()
        for page in self.writer2:
            pdf_writer2.addPage(page)
        with Path(filename + "_2.pdf").open(mode="wb") as output_file:
            pdf_writer2.write(output_file)


file_path = Path.home() / "practice_files" / "Pride_and_Prejudice.pdf"
pride = PdfFileSplitter(file_path)
pride.split(150)
pride.write("Pride")
