import tkinter as tk
from PIL import Image, ImageTk
import random
from lista_slow import words_to_guess


def hide_word(word):
    return "_" * len(word)


uncovered_word = random.choice(words_to_guess)
hidden_word = hide_word(uncovered_word)
mistakes = 0
error = False


def start_game():
    quit_btn.grid_remove()
    header_img_lbl["image"] = photo_title
    global mistakes, uncovered_word, hidden_word
    uncovered_word = random.choice(words_to_guess)
    hidden_word = hide_word(uncovered_word)
    mistakes = 0
    add_letter_btn["text"] = "Sprawdź literę"
    guessed_word_lbl.grid(row=0, column=0)
    enter_letter.grid(row=2, column=0)
    enter_letter.focus_set()
    guessed_word_lbl["text"] = hidden_word
    add_letter_btn["command"] = show_letter
    description_lbl["text"] = "Zgadnij jakie to słowo ?"
    while mistakes < 4:
        description_lbl["text"] = "\u2193\u2193\u2193 Podaj literę \u2193\u2193\u2193"
        image_gameplay["image"] = gameplay_photos[mistakes]
        windows.wait_variable(var)
        var.set(False)
        guessed_word_lbl["text"] = hidden_word
        if uncovered_word == hidden_word:
            header_img_lbl["image"] = photo_win
            image_gameplay["image"] = photo_stage0
            description_lbl["text"] = "Brawo zgadłeś!!!"
            break
        elif mistakes == 4:
            header_img_lbl["image"] = photo_game_over
            image_gameplay["image"] = photo_stage4
            guessed_word_lbl["text"] = uncovered_word
            description_lbl["text"] = "Zobacz jakie to było proste :)"

    enter_letter.grid_remove()
    add_letter_btn["text"] = "Jeszcze raz!"
    quit_btn.grid(row=2, column=1)
    quit_btn["command"] = game_over
    add_letter_btn["command"] = start_game


def show_letter():
    global mistakes, uncovered_word, hidden_word, error
    var.set(True)
    letter = enter_letter.get()
    list_hidden_word = list(hidden_word)
    error = False
    for i in range(len(list_hidden_word)):
        if uncovered_word[i] == letter:
            list_hidden_word[i] = letter
        elif letter not in uncovered_word:
            photo = gameplay_photos[mistakes]
            image_gameplay["image"] = photo
            error = True
    if error:
        mistakes += 1
    hidden_word = "".join(list_hidden_word)
    enter_letter.delete(0, tk.END)
    enter_letter.focus_set()


def game_over():
    windows.destroy()


def start_new_game():
    start_game()


windows = tk.Tk()
windows.geometry()
windows.configure(bg="black")
windows.title("Wisielec")

var = tk.BooleanVar()
var.set(False)

header_frm = tk.Frame(master=windows, bg="black")

image = Image.open("./graphics/title_wisielec.png")
photo_title = ImageTk.PhotoImage(image)
stage0_img = Image.open("./graphics/0.png")
photo_stage0 = ImageTk.PhotoImage(stage0_img)
stage1_img = Image.open("./graphics/1_small.jpg")
photo_stage1 = ImageTk.PhotoImage(stage1_img)
stage2_img = Image.open("./graphics/2_small.png")
photo_stage2 = ImageTk.PhotoImage(stage2_img)
stage3_img = Image.open("./graphics/3_small.png")
photo_stage3 = ImageTk.PhotoImage(stage3_img)
stage4_img = Image.open("./graphics/4_final_small.png")
photo_stage4 = ImageTk.PhotoImage(stage4_img)
game_over_img = Image.open("./graphics/game_over.png")
photo_game_over = ImageTk.PhotoImage(game_over_img)
win_img = Image.open("./graphics/win.png")
photo_win = ImageTk.PhotoImage(win_img)

gameplay_photos = [photo_stage0, photo_stage1, photo_stage2, photo_stage3, photo_stage4]
header_img_lbl = tk.Label(windows, image=photo_title, bg="black")
header_img_lbl.pack(padx=10, pady=10, fill="both", expand=True)
header_frm.pack()

split_frm = tk.Frame(master=windows, bg="black")
split_frm.pack()

pad_frm = tk.Frame(master=split_frm, width=10, bg="black")
pad_frm.grid(row=0, column=0)

game_frm = tk.Frame(master=split_frm, bg="#780512", border=5, pady=20)
game_frm.grid(row=0, column=1)

guessed_word_lbl = tk.Label(master=game_frm, width=15, height=1, text="____s__", font=("Calibri", 20, "bold"), border=4,
                            relief="ridge")

description_lbl = tk.Label(master=game_frm, width=50, fg="black", text="Witaj graczu, czy chcesz zagrać?",
                           bg="#780512", font=("Arial", 12, "bold"))
description_lbl.grid(row=1, column=0, pady=5)

enter_letter = tk.Entry(master=game_frm, width=1, font=("Calibri", 15, "bold"), relief="ridge", border=5,
                        justify="center")

add_letter_btn = tk.Button(master=game_frm, text="Rozpocznij grę", bg="black", fg="#FF7025", padx=5, pady=5,
                           command=start_game)
add_letter_btn.grid(row=2, column=2)

quit_btn = tk.Button(master=game_frm, text="Zakończ", bg="black", fg="#FF7025", padx=5, pady=5)

img_frm = tk.Frame(master=split_frm, padx=30, pady=20, bg="black")
img_frm.grid(row=0, column=2, sticky="e")

image_gameplay = tk.Label(master=img_frm, image=gameplay_photos[0])
image_gameplay.pack()

windows.mainloop()
