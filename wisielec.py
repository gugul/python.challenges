import random


def hide_word(word):
    return "_" * len(word)


def show_letter(letter):
    uncovered_word = list(words[0])
    for i in range(len(uncovered_word)):
        if words[1][i] == letter:
            uncovered_word[i] = letter
    return "".join(uncovered_word)


def generate_word():
    generated_word = random.choice(words_in_game)
    hidden_word = hide_word(generated_word)
    return [hidden_word, generated_word]


words_in_game = ["taboret", "komputer", "gospodarstwo", "uniwersytet"]

hangman_animation = ["""          


              PIERWSZY BŁĄD!



    ======""", """   
        \|
         |        
         |     POSTARAJ SIĘ BARDZIEJ!
         |
         |
         |
    ======""", """    _ _ _
        \|
         |
         |     TO TWOJA OSTATNIA SZANSA!
         |
         |
         |
    ======""", """    _ _ _
     |  \|
     o   |
    /|\  |     PRZEGRAŁEŚ!!!
     |   |
    / \  |
         |
    ======""", """    

    \o/   
     |        HURRA!!!
     |   
    / \  

    """]

if __name__ == '__main__':
    play_again = "tak"
    while play_again == "tak":
        words = generate_word()

        print("""
        Zgadnij jakie to słowo?
        
        Uważaj, możesz się pomylić tylko 4 razy!
        """)
        mistakes = 0
        while mistakes < 4:
            entered_letter = input(f"""        {words[0]}        Podaj literę: 
             """)
            words[0] = show_letter(entered_letter)
            # print(f"     {words[0]}")
            if entered_letter not in words[1]:
                print(hangman_animation[mistakes])
                mistakes += 1
            elif words[0] == words[1]:
                print("Brawo! Zgadłeś")
                print(hangman_animation[4])
                break

        play_again = input("Czy chcesz zagrać jeszcze raz? (tak/nie): ".lower())
        if play_again == "tak":
            play_again = "tak"
        elif play_again == "nie":
            print("Do zobaczenia następnym razem!")
            exit()
        else:
            print("Wpisz tak/nie")

