import random

capitals_dict = {
    'Alabama': 'Montgomery',
    'Alaska': 'Juneau',
    'Arizona': 'Phoenix',
    'Arkansas': 'Little Rock',
    'California': 'Sacramento',
    'Colorado': 'Denver',
    'Connecticut': 'Hartford',
    'Delaware': 'Dover',
    'Florida': 'Tallahassee',
    'Georgia': 'Atlanta',
}


def random_state_extraction(dictionary):
    states = list(dictionary.keys())
    if states:
        random_state = random.choice(states)
        random_value = dictionary[random_state]
        return random_state, random_value.lower()
    else:
        return None


state, capital = random_state_extraction(capitals_dict)

while True:
    answer = input(f"What is the capital of {state}? ")
    if answer.lower() == capital:
        print("Correct")
        break
    elif answer.lower() == "exit":
        print("Goodbye")
        break
